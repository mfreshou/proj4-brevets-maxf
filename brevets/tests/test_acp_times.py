"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
"""
Nose tests for acp_times.py
"""
# Test if distance is negative
# Check if race distance is not valid
# Check if control point is > length of race
# Check if time is correct format
# Check if variables are correct type

import arrow
import acp_times
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)
log = logging.getLogger(__name__)
time_now = arrow.now()

def test_wronglength():
    """
    Checks if the race distance is in the list or not
    """
    assert (acp_times.open_time(24.0, 5000.0, time_now) == "Invalid inputs")
    assert (acp_times.open_time(24, 500.0, time_now) == acp_times.open_time(24.0, 500.0, time_now))
    assert (acp_times.close_time(24.0, 6.0, time_now) == "Invalid inputs")
    assert (acp_times.close_time(24, 600.0, time_now) == acp_times.close_time(24.0, 600.0, time_now))

def test_validDistance():
    """
    Checks if the controle point distance is good
    """
    assert (acp_times.open_time(2451, 500.0, time_now) == "Invalid inputs")
    assert (acp_times.open_time(24, 500.0, time_now) == acp_times.open_time(24.0, 500.0, time_now))
    assert (acp_times.close_time(800, 600.0, time_now) == "Invalid inputs")
    assert (acp_times.close_time(24, 600.0, time_now) == acp_times.close_time(24, 600.0, time_now))

def tests_timeFormat():
    """
    Checks if the time format is correct or not
    """
    assert (acp_times.open_time(24.0, 500.0, 50) == "Invalid inputs")
    assert (acp_times.open_time(24.0, 500.0, time_now) == acp_times.open_time(24.0, 500.0, time_now))
    assert (acp_times.close_time(80.0, 600.0, 50) == "Invalid inputs")
    assert (acp_times.close_time(24.0, 600.0, time_now) == acp_times.close_time(24.0, 600.0, time_now))

def tests_negativeVal():
    """
    Checks if the controle point distance is negative
    """
    assert (acp_times.open_time(-24.0, 500.0, time_now) == "Invalid inputs")
    assert (acp_times.open_time(24.0, 500.0, time_now) == acp_times.open_time(24.0, 500.0, time_now))
    assert (acp_times.close_time(-30.0, 600.0, time_now) == "Invalid inputs")
    assert (acp_times.close_time(24.0, 600.0, time_now) == acp_times.close_time(24.0, 600.0, time_now))

def tests_vartype():
    """
    Checks if variables are right type
    """
    assert (acp_times.open_time("-24.0", "500.0", 5) == "Invalid inputs")
    assert (acp_times.close_time("-30.0", "hello", 18) == "Invalid inputs")

