"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, url_for, redirect, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 5000, type=float) # changed default val
    # 5000 is way longer than any race
    start_date = request.args.get('sdate', type=str)
    start_time = request.args.get('stime', type=str)
    place = request.args.get('local', type=str)
    # check the race distance to see if it
    # getting the selected item from the menu
    brev = request.args.get('brev', type=str)
    brev = float(brev)
    # making it to an ISO 8601 string for arrow to get
    # comb_time = "" + start_date + "T" + start_time + ":00.000+00:00"
    # app.logger.debug("comb_time={}".format(comb_time))
    # the actual arrow time string
    a_time = arrow.get(start_date + "T" + start_time)
    app.logger.debug("current time= {}".format(arrow.now()))
    app.logger.debug("a_time= {}".format(a_time))
    app.logger.debug("km= {}".format(km))
    app.logger.debug("Loaction= {}".format(place))
    app.logger.debug("start_date= {}".format(start_date))
    app.logger.debug("start_time= {}".format(start_time))
    app.logger.debug("brev= {}".format(brev))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brev, a_time)
    close_time = acp_times.close_time(km, brev, a_time)
    point = "Good"
    if(open_time == "Invalid inputs" or close_time == "Invalid inputs"):
        point = "Bad"
    app.logger.debug("open_time= {}".format(open_time))
    app.logger.debug("close_time= {}".format(close_time))
    app.logger.debug("point= {}".format(point))
    result = {"open": open_time, "close": close_time, "gp": point}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
