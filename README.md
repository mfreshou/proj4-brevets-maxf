# Project 4: Brevet time calculator with Ajax VERSION 2

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Version Author: Max Freshour

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation that you will do will fill in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. You'll extend that functionality as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* You'll also implement the logic in acp_times.py based on the algorithm given above. I will leave much of the design to you. You'll turn the implementation that you do. See below for more information.

## Testing

A suite of nose test cases is a requirement of this project. Design the test cases based on an interpretation of rules here (https://rusa.org/pages/acp-brevet-control-times-calculator). Be sure to test your test cases: You can use the current brevet time calculator (https://rusa.org/octime_acp.html) to check that your expected test outputs are correct. While checking these values once is a manual operation, re-running your test cases should be automated in the usual manner as a Nose test suite.

To make automated testing more practical, your open and close time calculations should be in a separate module. Because I want to be able to use my test suite as well as yours, I will require that module be named acp_times.py and contain the two functions I have included in the skeleton code (though revised, of course, to return correct results).

We should be able to run your test suite by changing to the "brevets" directory and typinng "nosetests". All tests should pass. You should have at least 5 test cases, and more importantly, your test cases should be chosen to distinguish between an implementation that correctly interprets the ACP rules and one that does not.

## acp_times

acp_times is where the logic of the calculator is. The main functions are open_time and close_time. Both have signitures of (control_dist_km (float), brevet_dist_km (float), brevet_start_time (arrow.arrow.Arrow))

These functions are set with specific speed intervals for 0-200 km, 200-400 km, 400-600 km, and 600-1000 km. These speed intervals are different in the two functions as the open times require the max speed set by rusa and the close times require the min speed for those interval. Also set are the end times for specific distances as set by rusa.

If an input is invalid the functions will return strings of "Invalid input", otherwise it will return the proper open and close times. The default value for a controle point has been set to 5000 km as that is significantly longer then the longest brevet distance allowed.

Inspired by (https://rusa.org/octime_acp.html) the controle point given is not allowed to extend more than 20% beyond the race length and will produce an error if done otherwise.

The race distances are also set as there are specific distances allowed for the races.

## sources

Sources used:
https://stackoverflow.com/questions/17855842/moment-js-utc-gives-wrong-date
https://rusa.org/pages/acp-brevet-control-times-calculator
https://learn.jquery.com/using-jquery-core/faq/how-do-i-get-the-text-value-of-a-selected-option/
https://arrow.readthedocs.io/en/latest/
https://momentjs.com/docs/
https://www.geeksforgeeks.org/round-function-python/
https://stackoverflow.com/questions/14549405/python-check-instances-of-classes
https://stackoverflow.com/questions/5142418/what-is-the-use-of-assert-in-python